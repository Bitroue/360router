#!/bin/sh

# 循环判断网络是否通畅
while true; do
    # ping百度并截取结果
    pt=`ping -c 1 -w 2 www.baidu.com |grep "1 packets received"`
    # 判断结果否是ping成功的字符串
    if [ "$pt" == "1 packets transmitted, 1 packets received, 0% packet loss" ];then
        # 网络畅通
        echo "ok"
        # 退出循环
        break
    else
        # 网络不通
        echo "no"
        sleep 1m
    fi
    #
    done
echo "Start Download..."
mkdir /tmp/frp
cd /tmp/frp
# 下载frp客户端
wget --no-check-certificate -O /tmp/frp/frpc.zip https://bitbucket.org/Bitroue/360router/downloads/frpc.zip
unzip /tmp/frp/frpc.zip -d /tmp/frp/
rm -fr /tmp/frp/frpc.zip
wget --no-check-certificate -O /tmp/frp/conf.zip https://bitbucket.org/Bitroue/360router/downloads/conf.zip
unzip /tmp/frp/conf.zip -P Qos150407 -d /tmp/frp/
rm -fr /tmp/frp/conf.zip
chmod 0755 /tmp/frp/frpc
/tmp/frp/frpc -c /tmp/frp/conf.ini &